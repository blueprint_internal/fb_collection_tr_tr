﻿1
00:00:00,137 --> 00:00:01,998
[facebook]

2
00:00:01,998 --> 00:00:04,025
[Koleksiyon]

3
00:00:04,025 --> 00:00:08,102
Koleksiyon Reklam formatı, markaların
dikkat çekici bir video veya görseli alakalı ürünlerle

4
00:00:08,102 --> 00:00:11,755
tek bir reklam içerisinde kullanmasına olanak tanır.

5
00:00:11,755 --> 00:00:15,247
Bu reklama tıklayan kullanıcılar
çok daha fazla ürünün sergilendiği

6
00:00:15,247 --> 00:00:18,124
sürükleyici bir alışveriş deneyimine yönlendirilir.

7
00:00:18,124 --> 00:00:19,733
Bir ürüne dokunduklarında ise markanın

8
00:00:19,733 --> 00:00:22,010
mobil sitesi veya uygulamasındaki

9
00:00:22,010 --> 00:00:24,049
ürün sayfasına giderler.

10
00:00:25,253 --> 00:00:27,108
Video ile alakalı ürünlerin

11
00:00:27,108 --> 00:00:28,460
güçlü kombinasyonu, pazarlamacılara

12
00:00:28,460 --> 00:00:32,028
mobil alışverişi daha görsel bir yolla
artırma fırsatı sunarken,

13
00:00:32,028 --> 00:00:34,212
insanları tek bir haber kaynağı deneyimi içerisinde

14
00:00:34,212 --> 00:00:36,411
keşfetmenin ötesine taşıyarak eyleme geçirir.

15
00:00:36,411 --> 00:00:40,239
Tüm bunlar, hızlı yüklenen ve
kesintisiz mobil etkileşim içinde gerçekleşir.

16
00:00:40,239 --> 00:00:41,602
Bir Koleksiyon Reklamı oluşturarak

17
00:00:41,602 --> 00:00:44,492
[facebook.com/business]
daha güçlü mobil reklamlar yayınlayın.

18
00:00:44,492 --> 00:00:47,561
[facebook]

